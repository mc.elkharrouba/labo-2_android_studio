package com.example.labo_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.admin_.MainActivity;
import com.example.dao2.dbSelectAllStudent;
import com.example.dao2.dbSelectAllactivité;

public class MainPanel extends AppCompatActivity {

    private Button btn;
    private Button btn2;
    private Button btn3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_panel);
        SharedPreferences userDetails = getSharedPreferences("userdetails", MODE_PRIVATE);
        SharedPreferences.Editor edit = userDetails.edit();

        final student std = (student) getIntent().getSerializableExtra("user");
        edit.putString("id", String.valueOf(std.getId()));
        //edit.putString("password", password.getText().toString().trim());
        edit.apply();

        this.btn = (Button) findViewById(R.id.btn1);
        this.btn2 = (Button) findViewById(R.id.btn2);
        this.btn3 = (Button) findViewById(R.id.btn3);
        this.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainPanel.this,mainUpdated.class);
                intent.putExtra("user",std);
                startActivity(intent);
            }
        });


        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbSelectAllactivité activity = new dbSelectAllactivité(MainPanel.this);
                activity.execute();
            }
        });

        this.btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbSelectAllStudent db = new dbSelectAllStudent(MainPanel.this);
                db.execute();
            }
        });

    }
}