package com.example.labo_2;

import androidx.appcompat.app.AppCompatActivity;
import  com.example.dao.*;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.dao.dbLogin;

public class MainLogin extends AppCompatActivity {
    private EditText user;
    private EditText pass;
    private Button login;
    private Button sign;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);

        this.user = (EditText) findViewById(R.id.username);
        this.pass = (EditText) findViewById(R.id.password);
        this.login = (Button) findViewById(R.id.log);
        this.sign = (Button) findViewById(R.id.sign);


        this.login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    dbLogin login = new dbLogin(MainLogin.this);
                    login.execute(user.getText().toString(),pass.getText().toString());
            }
        });

        this.sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainLogin.this,MainInscription.class);
                startActivity(intent);
            }
        });
    }
}