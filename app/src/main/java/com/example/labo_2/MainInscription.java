package com.example.labo_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.dao.dbUserInsert;

public class MainInscription extends AppCompatActivity {
    private EditText nom;
    private EditText prenom;
    private EditText work;
    private EditText username;
    private EditText pass;
    private Button signin;
    private Button signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_inscription);


        this.nom = (EditText) findViewById(R.id.nom);
        this.prenom = (EditText) findViewById(R.id.prenom);
        this.work = (EditText) findViewById(R.id.work);
        this.username = (EditText) findViewById(R.id.username);
        this.pass = (EditText) findViewById(R.id.pass);

        this.signin = (Button) findViewById(R.id.signin);
        this.signup = (Button) findViewById(R.id.singup);

        this.signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainInscription.this,MainLogin.class);
                startActivity(intent);
            }
        });

        this.signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbUserInsert insert = new dbUserInsert(MainInscription.this);
                insert.execute(nom.getText().toString(),prenom.getText().toString(),work.getText().toString(),username.getText().toString(),pass.getText().toString());
            }
        });
    }
}