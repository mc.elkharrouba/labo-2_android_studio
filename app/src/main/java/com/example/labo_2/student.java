package com.example.labo_2;

import java.io.Serializable;

public class student implements Serializable {

    private  int id;
    private  String nom;
    private String prenom;
    private String travaille;
    private String username;
    private String password;

    public student() {

    }
    public student(int id, String nom, String prenom, String travaille, String username, String password) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.travaille = travaille;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTravaille() {
        return travaille;
    }

    public void setTravaille(String travaille) {
        this.travaille = travaille;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
