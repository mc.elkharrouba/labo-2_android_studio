package com.example.labo_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.dao.dbUserUpdate;

public class mainUpdated extends AppCompatActivity {

    private EditText nom;
    private EditText prenom;
    private EditText work;
    private EditText username;
    private EditText pass;
    private Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_updated);

        this.nom = (EditText) findViewById(R.id.nom);
        this.prenom = (EditText) findViewById(R.id.prenom);
        this.work = (EditText) findViewById(R.id.work);
        this.username = (EditText) findViewById(R.id.username);
        this.pass = (EditText) findViewById(R.id.pass);

        this.submit = (Button) findViewById(R.id.submit);



        final student std = (student) getIntent().getSerializableExtra("user");

        this.nom.setText(std.getNom().toString());
        this.prenom.setText(std.getPrenom().toString());
        this.work.setText(std.getTravaille().toString());
        this.username.setText(std.getUsername().toString());
        this.pass.setText(std.getPassword().toString());

        this.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbUserUpdate update = new dbUserUpdate(mainUpdated.this);
                update.execute(nom.getText().toString(),prenom.getText().toString(),work.getText().toString(),username.getText().toString(),pass.getText().toString(),String.valueOf(std.getId()));
            }
        });

    }
}