package com.example.dao;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.widget.EditText;

import com.example.labo_2.MainPanel;
import com.example.labo_2.student;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import static android.content.Context.MODE_PRIVATE;

public class dbLogin extends AsyncTask {

    private String FileName;

    private Context context;
    private AlertDialog alert;
    private student user;
    public dbLogin(Context c)
    {
        this.context = c;
    }

    @Override
    protected void onPreExecute() {
        this.alert = new AlertDialog.Builder(context).create();
        this.user = new student();
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        String cible = "http://192.168.0.170/android/Login.php";

        try {
            URL url  = new URL(cible);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");

            OutputStream outs = con.getOutputStream();
            BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs,"utf-8"));
            String msg = URLEncoder.encode("username","utf-8")+
                    "="+URLEncoder.encode((String)objects[0],"utf-8")+
                    "&"+URLEncoder.encode("password","utf-8")+
                    "="+URLEncoder.encode((String)objects[1],"utf-8");

            bufw.write(msg);
            bufw.flush();
            bufw.close();
            outs.close();

            InputStream ins = con.getInputStream();
            BufferedReader bufr= new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
            String line;
            StringBuffer sbuff = new StringBuffer();

            while((line = bufr.readLine()) != null)
            {
                sbuff.append(line+"\n");

            }
            //Log.d("hey : ",sbuff.toString());

            JSONArray json = new JSONArray(sbuff.toString());
            for(int i=0; i < json.length(); i++)
            {
                JSONObject equip = json.getJSONObject(i);
                user.setId(equip.getInt("IdUtilisateur"));
                user.setNom(equip.getString("nom"));
                user.setPrenom(equip.getString("prenom"));
                user.setTravaille(equip.getString("travail"));
                user.setUsername(equip.getString("user"));
                user.setPassword(equip.getString("pw"));

            }


        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }


        return this.user;

    }


    @Override
    protected void onPostExecute(Object o) {
        if(user.getUsername() != null && user.getPassword() != null)
        {
            Intent intent = new Intent(context, MainPanel.class);
            intent.putExtra("user",this.user);
            System.out.println(user.getNom());
            System.out.println(user.getPassword());
            context.startActivity(intent);


        }
        else {
            this.alert.setMessage("Login ou password Incorrect");
            this.alert.show();
        }
    }
}
