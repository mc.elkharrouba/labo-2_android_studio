package com.example.dao2;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.example.admin_.ActivityStudent;
import com.example.admin_.MainActivity;
import com.example.labo_2.student;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class dbSelectAllStudent extends AsyncTask {

    private Context context;
    private ArrayList<student> list ;

    public dbSelectAllStudent(Context c)
    {
        this.context = c;
        this.list = new ArrayList<student>();
    }


    @Override
    protected void onPreExecute() {


    }

    @Override
    protected Object doInBackground(Object[] objects) {

        String cible = "http://192.168.0.170/android/all_student.php";
        try
        {
            URL url  = new URL(cible);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("GET");

            OutputStream outs = con.getOutputStream();

            outs.close();

            InputStream ins = con.getInputStream();
            BufferedReader bufr= new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
            String line;
            StringBuffer sbuff = new StringBuffer();

            while((line = bufr.readLine()) != null)
            {
                sbuff.append(line+"\n");

            }
            //Log.d("hey : ",sbuff.toString());

            JSONArray json = new JSONArray(sbuff.toString());
            for(int i=0; i < json.length(); i++)
            {
                JSONObject equip = json.getJSONObject(i);
                this.list.add(new student( equip.getInt("IdUtilisateur") , equip.getString("nom") ,equip.getString("prenom"),equip.getString("travail"), equip.getString("user") , equip.getString("pw") ));
            }


        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }


        return this.list;
    }

    @Override
    protected void onPostExecute(Object o) {
        if(o != null)
        {
            Intent intent = new Intent(context, ActivityStudent.class);
            intent.putExtra("List",this.list);
            context.startActivity(intent);
        }
    }
}
