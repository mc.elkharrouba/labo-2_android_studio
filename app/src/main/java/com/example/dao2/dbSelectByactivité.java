package com.example.dao2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.example.admin_.MainActivity;
import com.example.admin_.activite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class dbSelectByactivité extends AsyncTask {

    private Context context;
    activite act;
    SharedPreferences useractivity;

    public dbSelectByactivité(Context c)
    {
        this.context = c;
        useractivity = c.getSharedPreferences("useractivity", MODE_PRIVATE);
    }
    @Override
    protected void onPreExecute() {

        this.act = new activite();

    }

    @Override
    protected Object doInBackground(Object[] objects) {

        String cible = "http://192.168.0.170/android/all_activity.php";
        try
        {
            URL url  = new URL(cible);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("GET");

            OutputStream outs = con.getOutputStream();

            outs.close();

            InputStream ins = con.getInputStream();
            BufferedReader bufr= new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
            String line;
            StringBuffer sbuff = new StringBuffer();

            while((line = bufr.readLine()) != null)
            {
                sbuff.append(line+"\n");


            }
            //Log.d("hey : ",sbuff.toString());

            JSONArray json = new JSONArray(sbuff.toString());

            JSONObject equip =json.getJSONObject(Integer.parseInt(objects[0].toString()));
            //this.list.add(new activite(,equip.getString("name_activity"),equip.getString("desc_activity"),equip.getString("place_activity")));
            this.act.setId(equip.getInt("ID_activity"));



        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }


        return this.act;
    }

    @Override
    protected void onPostExecute(Object o) {

        System.out.println("my id"+ this.act.getId());
        if( o != null)
        {
            SharedPreferences.Editor edit2 = useractivity.edit();
            edit2.putString("id_activity",String.valueOf(this.act.getId()));
        }
    }
}
