package com.example.dao2;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.example.admin_.MainActivity;
import com.example.admin_.activite;
import com.example.labo_2.student;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class dbSubscribeIntoActivité extends AsyncTask {

    private String FileName;
    private Context context;
    private AlertDialog alert;
    private student user;
    public dbSubscribeIntoActivité(Context c)
    {
        this.context = c;
    }

    @Override
    protected void onPreExecute() {
        this.alert = new AlertDialog.Builder(context).create();
        this.user = new student();
    }

    @Override
    protected String doInBackground(Object[] objects) {

        String cible = "http://192.168.0.170/android/subscribtion_activiy.php";

        try {
            URL url  = new URL(cible);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");

            OutputStream outs = con.getOutputStream();
            BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs,"utf-8"));
            String msg = URLEncoder.encode("id_user","utf-8")+
                    "="+URLEncoder.encode((String)objects[0],"utf-8")+
                    "&"+URLEncoder.encode("id_activity","utf-8")+
                    "="+URLEncoder.encode((String)objects[1],"utf-8");


            bufw.write(msg);
            bufw.flush();
            bufw.close();
            outs.close();

            InputStream ins = con.getInputStream();
            BufferedReader bufr= new BufferedReader(new InputStreamReader(ins,"iso-8859-1"));
            String line;
            StringBuffer sbuff = new StringBuffer();

            while((line = bufr.readLine()) != null)
            {
                sbuff.append(line+"\n");

            }

            return sbuff.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


    @Override
    protected void onPostExecute(Object o) {
        if(o != null) {
            this.alert.setMessage("You subscribed to this activity!");
            this.alert.show();
        }
    }
}
