package com.example.admin_;

import java.io.Serializable;

public class activite  implements Serializable {


    private int id;
    private String name;
    private  String description;
    private String place;

    public activite() {

    }
    public activite(int id, String name, String description, String place) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.place = place;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
