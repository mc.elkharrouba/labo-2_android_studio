package com.example.admin_;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.labo_2.R;
import com.example.labo_2.student;

import java.util.ArrayList;

class costumeAdpterStudent extends BaseAdapter {


    private Context context;
    private int resAnInt;
    public ArrayList<student> listnewsDataAdpater ;

    public costumeAdpterStudent(Context c,int r ,ArrayList<student>  listnewsDataAdpater) {
        this.context = c;
        this.resAnInt = r ;
        this.listnewsDataAdpater=listnewsDataAdpater;
    }


    @Override
    public int getCount() {
        return listnewsDataAdpater.size();
    }

    @Override
    public String getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater mInflater = LayoutInflater.from(context);
        View myView = mInflater.inflate(resAnInt, null);

        final   student u = listnewsDataAdpater.get(position);

        TextView id = (TextView) myView.findViewById(R.id.id_act);
        id.setText(String.valueOf(u.getId()));

        TextView nom = (TextView) myView.findViewById(R.id.name);
        nom.setText(u.getNom().toString());

        TextView desc = (TextView) myView.findViewById(R.id.desc);
        desc.setText(u.getPrenom().toString());


        return myView;
    }
}
