package com.example.admin_;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.labo_2.R;

import java.util.ArrayList;

class costumeAdpter extends BaseAdapter {


    private Context context;
    private int resAnInt;
    public ArrayList<activite> listnewsDataAdpater ;

    public costumeAdpter(Context c,int r ,ArrayList<activite>  listnewsDataAdpater) {
        this.context = c;
        this.resAnInt = r ;
        this.listnewsDataAdpater=listnewsDataAdpater;
    }


    @Override
    public int getCount() {
        return listnewsDataAdpater.size();
    }

    @Override
    public String getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater mInflater = LayoutInflater.from(context);
        View myView = mInflater.inflate(resAnInt, null);

        final   activite u = listnewsDataAdpater.get(position);

        TextView id = (TextView) myView.findViewById(R.id.id_act);
        id.setText(String.valueOf(u.getId()));

        TextView nom = (TextView) myView.findViewById(R.id.name);
        nom.setText(u.getName().toString());

        TextView desc = (TextView) myView.findViewById(R.id.desc);
        desc.setText(u.getDescription().toString());

        TextView place = (TextView) myView.findViewById(R.id.place);
        place.setText(u.getPlace().toString());

        return myView;
    }
}
