package com.example.admin_;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.example.labo_2.R;
import com.example.labo_2.student;

import java.util.ArrayList;

public class ActivityStudent extends AppCompatActivity {
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        listView = (ListView) findViewById(R.id.list_u);

        ArrayList<student> list = (ArrayList<student>) getIntent().getSerializableExtra("List");

        costumeAdpterStudent adapter3 = new costumeAdpterStudent(ActivityStudent.this, R.layout.layout_cost_2 ,list);
        this.listView.setAdapter(adapter3);
    }
}