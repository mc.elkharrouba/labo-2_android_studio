package com.example.admin_;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.dao2.dbSelectByactivité;
import com.example.dao2.dbSubscribeIntoActivité;
import com.example.labo_2.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private AlertDialog.Builder alert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alert = new AlertDialog.Builder(this);
        listView = (ListView) findViewById(R.id.mylist);
        final SharedPreferences userDetails = getSharedPreferences("userdetails", MODE_PRIVATE);
        final SharedPreferences useractivity = getSharedPreferences("useractivity", MODE_PRIVATE);

        ArrayList<activite> list = (ArrayList<activite>) getIntent().getSerializableExtra("List");

        costumeAdpter adapter2 = new costumeAdpter(MainActivity.this,R.layout.layout_cost_1,list);
        this.listView.setAdapter(adapter2);

        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                alert.setMessage("Do you want to subscribe into this activity ?");
                dbSelectByactivité select = new dbSelectByactivité(MainActivity.this);
                select.execute(String.valueOf(position));

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dbSubscribeIntoActivité activité = new dbSubscribeIntoActivité(MainActivity.this);
                        String id = userDetails.getString("id", "");
                        String id_ac = useractivity.getString("id_activity","");
                        activité.execute(String.valueOf(position+1),id);
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog al = alert.create();
                al.show();
            }
        });

    }
}